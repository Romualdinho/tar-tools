package extract;

import tartools.StdTarTools;
import tartools.TarTools;

import java.io.IOException;
import java.util.Arrays;

public class Extract {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("USAGE : ./extract.sh archive.tar target options");
            System.exit(0);
        }

        try {
            TarTools tools = new StdTarTools();
            tools.extract(args[0], args[1], String.join(" ", Arrays.copyOfRange(args, 2, args.length)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}