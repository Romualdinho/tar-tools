package list;

import tartools.StdTarTools;
import tartools.TarTools;

import java.io.IOException;
import java.util.Arrays;

public class List {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("USAGE : ./list.sh archive.tar options");
            System.exit(0);
        }

        try {
            TarTools tools = new StdTarTools();
            tools.list(args[0], String.join(" ", Arrays.copyOfRange(args, 1, args.length)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}