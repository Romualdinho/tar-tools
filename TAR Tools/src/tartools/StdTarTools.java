package tartools;

import tartools.archive.StdTarArchiveBuilder;
import tartools.archive.TarArchive;
import tartools.archive.TarArchiveBuilder;
import tartools.archive.TarArchiveDirector;
import tartools.archive.visitors.ExtractVisitor;
import tartools.archive.visitors.ListVisitor;
import tartools.options.ExtractOptionFactory;
import tartools.options.ListOptionFactory;
import tartools.options.TarOptionFactory;
import tartools.options.TarOptionWrapper;
import tartools.options.filter.TarFilterOption;
import tartools.options.standard.TarStandardOption;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class StdTarTools implements TarTools {
    @Override
    public void list(String path, String options) throws IOException {
        list(new File(path), toTarOptions(new ListOptionFactory(), options));
    }

    @Override
    public void list(File tar, TarOptionWrapper options) throws IOException {
        TarArchiveBuilder builder = new StdTarArchiveBuilder();
        TarArchiveDirector.constructTarArchive(tar, builder);

        TarArchive archive = builder.getResult();
        archive.accept(new ListVisitor(options));
    }

    @Override
    public void extract(String path, String target, String options) throws IOException {
        extract(new File(path), new File(target), toTarOptions(new ExtractOptionFactory(), options));
    }

    @Override
    public void extract(File tar, File directory, TarOptionWrapper options) throws IOException {
        TarArchiveBuilder builder = new StdTarArchiveBuilder();
        TarArchiveDirector.constructTarArchive(tar, builder);

        TarArchive archive = builder.getResult();
        archive.accept(new ExtractVisitor(directory, options));
    }

    @Override
    public TarOptionWrapper toTarOptions(TarOptionFactory factory, String optionsLine) {
        // On split la ligne de commande afin d'isoler les différentes options
        String[] splitByOption = optionsLine.trim().split("^-|[ ]-");

        // Collection d'options standard (ex : -v)
        Set<TarStandardOption> options = new LinkedHashSet<>();

        // Collection d'options de filtrage (ex : -n, -nx, -p)
        Set<TarFilterOption> filters = new LinkedHashSet<>();

        // Pour chaque option différente
        for (String opt : splitByOption) {
            // Si le texte est vide, on passe
            if (opt.isEmpty()) {
                continue;
            }

            // On split l'option afin de récupérer le nombre d'arguments
            String[] splitByWords = opt.trim().split(" ");

            // Nom de l'option
            String name = splitByWords[0];

            // Si l'option a plusieurs paramètres, il s'agit d'un filtre
            if (splitByWords.length > 1) {
                filters.add(factory.createFilterOption(name, Arrays.copyOfRange(splitByWords, 1, splitByWords.length)));
            } else {
                options.add(factory.createStandardOption(name));
            }
        }

        return new TarOptionWrapper(options, filters);
    }
}