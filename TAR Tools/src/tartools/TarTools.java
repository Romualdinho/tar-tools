package tartools;

import tartools.options.TarOptionFactory;
import tartools.options.TarOptionWrapper;

import java.io.File;
import java.io.IOException;

public interface TarTools {
    void list(String path, String options) throws IOException;
    void list(File tar, TarOptionWrapper options) throws IOException;

    void extract(String path, String target, String options) throws IOException;
    void extract(File tar, File directory, TarOptionWrapper options) throws IOException;

    TarOptionWrapper toTarOptions(TarOptionFactory factory, String optionsLine);
}