package tartools.archive;

import tartools.archive.files.FileRepresentation;
import tartools.archive.visitors.TarVisitor;

import java.io.File;
import java.util.Set;

public class StdTarArchive implements TarArchive {

    private File archive;
    private Set<FileRepresentation> files;

    public StdTarArchive(File archive, Set<FileRepresentation> files) {
        if (!archive.exists()) {
            throw new IllegalArgumentException("Archive file doesn't exist");
        }

        if (!archive.getName().endsWith(".tar")) {
            throw new IllegalArgumentException("Archive file must be TAR (.tar) file");
        }

        this.archive = archive;
        this.files = files;
    }

    @Override
    public File getArchive() {
        return archive;
    }

    @Override
    public Set<FileRepresentation> getFiles() {
        return files;
    }

    @Override
    public void accept(TarVisitor visitor) {
        for (FileRepresentation file : files) {
            file.accept(visitor);
        }
    }
}