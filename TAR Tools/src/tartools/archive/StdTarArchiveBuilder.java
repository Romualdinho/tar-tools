package tartools.archive;

import tartools.archive.files.DirectoryFileRepresentation;
import tartools.archive.files.FileRepresentation;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class StdTarArchiveBuilder implements TarArchiveBuilder {

    private File archive;

    private Set<FileRepresentation> rootFiles;
    private LinkedList<DirectoryFileRepresentation> openedDirectories;

    public StdTarArchiveBuilder() {
        rootFiles = new LinkedHashSet<>();
        openedDirectories = new LinkedList<>();
    }

    @Override
    public void reset() {
        archive = null;
        rootFiles.clear();
        openedDirectories.clear();
    }

    @Override
    public void setArchive(File archive) {
        this.archive = archive;
    }

    @Override
    public void startDirectory(DirectoryFileRepresentation directory) {
        if (openedDirectories.size() == 0) {
            rootFiles.add(directory);
        } else {
            openedDirectories.peekLast().addFile(directory);
        }

        openedDirectories.addLast(directory);
    }

    @Override
    public void addFile(FileRepresentation file) {
        if (openedDirectories.size() == 0) {
            rootFiles.add(file);
        } else {
            openedDirectories.peekLast().addFile(file);
        }
    }

    @Override
    public void endDirectory() {
        if (openedDirectories.size() == 0) {
            throw new IllegalStateException("No directory where started");
        }

        openedDirectories.removeLast();
    }

    @Override
    public TarArchive getResult() {
        if (archive == null || rootFiles.size() == 0) {
            throw new IllegalArgumentException();
        }

        return new StdTarArchive(archive, rootFiles);
    }
}