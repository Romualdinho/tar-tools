package tartools.archive;

import tartools.archive.files.FileRepresentation;
import tartools.archive.visitors.TarVisitor;

import java.io.File;
import java.util.Set;

public interface TarArchive {
    File getArchive();
    Set<FileRepresentation> getFiles();
    void accept(TarVisitor visitor);
}