package tartools.archive;

import tartools.archive.files.DirectoryFileRepresentation;
import tartools.archive.files.FileRepresentation;

import java.io.File;

public interface TarArchiveBuilder {
    void reset();

    void setArchive(File archive);
    void startDirectory(DirectoryFileRepresentation directory);
    void addFile(FileRepresentation file);
    void endDirectory();

    TarArchive getResult();
}