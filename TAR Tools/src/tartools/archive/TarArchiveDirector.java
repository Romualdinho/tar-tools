package tartools.archive;

import tartools.archive.files.DirectoryFileRepresentation;
import tartools.archive.files.HardLinkRepresentation;
import tartools.archive.files.StandardFileRepresentation;
import tartools.archive.files.SymbolicLinkRepresentation;
import tartools.archive.files.header.TarHeader;
import tartools.archive.files.header.TarHeaderFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

public class TarArchiveDirector {

    public static void constructTarArchive(File archive, TarArchiveBuilder builder) throws IOException {
        builder.reset();

        if (!archive.exists()) {
            throw new IllegalArgumentException("Archive file doesn't exist");
        }

        if (!archive.getName().endsWith(".tar")) {
            throw new IllegalArgumentException("Archive file must be TAR (.tar) file");
        }

        FileInputStream fis = new FileInputStream(archive);
        FileChannel channel = fis.getChannel();

        TarHeaderFactory headerFactory = new TarHeaderFactory();

        builder.setArchive(archive);

        int treeLevel = 0;
        do {
            // Lecture de l'entête
            byte buf[] = new byte[512];
            fis.read(buf, 0, buf.length);

            // On sort immédiatement si il n'y a plus d'entête à lire
            if (buf[0] == 0) {
                break;
            }

            // Construction de l'entête
            TarHeader header = headerFactory.createTarHeader(buf);

            // On vérifie si l'on vient de remonter dans l'arborescence de l'archive
            int currentTreeLevel = Paths.get(header.getName()).getNameCount();
            if (currentTreeLevel < treeLevel) {
                builder.endDirectory();
            }
            treeLevel = currentTreeLevel;

            switch (header.getType()) {
                case DIRECTORY:
                    builder.startDirectory(new DirectoryFileRepresentation(header, archive, channel.position()));
                    break;
                case STANDARD_FILE:
                    builder.addFile(new StandardFileRepresentation(header, archive, channel.position()));
                    break;
                case SYMBOLIC_LINK:
                    builder.addFile(new SymbolicLinkRepresentation(header, archive, channel.position()));
                    break;
                case HARD_LINK:
                    builder.addFile(new HardLinkRepresentation(header, archive, channel.position()));
                    break;
                default:
                    System.err.println("Fichier non supporté");
            }

            // On saute directement le fichier
            channel.position(channel.position() + header.getFileSize());

            // On termine la lecture du bloc afin de passer à l'entête suivante
            long modulo = header.getFileSize() % 512;
            if (modulo != 0) {
                channel.position(channel.position() + (512 - modulo));
            }
        } while (channel.position() < archive.length());

        fis.close();
    }
}