package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;

public abstract class AbstractFileRepresentation implements FileRepresentation {

    private TarHeader header;
    private File archive;
    private long position;


    public AbstractFileRepresentation(TarHeader header, File archive, long position) {
        this.header = header;
        this.archive = archive;
        this.position = position;
    }

    @Override
    public TarHeader getHeader() {
        return header;
    }

    @Override
    public File getArchive() {
        return archive;
    }

    @Override
    public long getFilePosition() {
        return position;
    }

    @Override
    public abstract void accept(TarVisitor visitor);
}