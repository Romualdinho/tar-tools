package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;

@SuppressWarnings("unused")
public class DirectoryFileRepresentation extends AbstractFileRepresentation {

    private Set<FileRepresentation> files;

    public DirectoryFileRepresentation(TarHeader header, File archive, long position) {
        super(header, archive, position);

        this.files = new LinkedHashSet<>();
    }

    public void addFile(FileRepresentation file) {
        files.add(file);
    }

    public void removeFile(FileRepresentation file) {
        files.remove(file);
    }

    @Override
    public void accept(TarVisitor visitor) {
        visitor.visitDirectoryFile(this);

        for (FileRepresentation file : files) {
            file.accept(visitor);
        }
    }

    public Set<FileRepresentation> getFiles() {
        return files;
    }
}