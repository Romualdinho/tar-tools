package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;

public interface FileRepresentation {
    TarHeader getHeader();
    File getArchive();
    long getFilePosition();
    void accept(TarVisitor visitor);
}