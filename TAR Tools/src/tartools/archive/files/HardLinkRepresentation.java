package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;

public class HardLinkRepresentation extends AbstractFileRepresentation {

    public HardLinkRepresentation(TarHeader header, File archive, long position) {
        super(header, archive, position);
    }

    @Override
    public void accept(TarVisitor visitor) {
        visitor.visitMaterialLinkFile(this);
    }
}