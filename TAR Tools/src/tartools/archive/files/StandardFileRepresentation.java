package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;

public class StandardFileRepresentation extends AbstractFileRepresentation {

    public StandardFileRepresentation(TarHeader header, File archive, long position) {
        super(header, archive, position);
    }

    @Override
    public void accept(TarVisitor visitor) {
        visitor.visitStandardFile(this);
    }
}