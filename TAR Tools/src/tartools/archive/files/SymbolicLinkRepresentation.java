package tartools.archive.files;

import tartools.archive.files.header.TarHeader;
import tartools.archive.visitors.TarVisitor;

import java.io.File;

public class SymbolicLinkRepresentation extends AbstractFileRepresentation {

    public SymbolicLinkRepresentation(TarHeader header, File archive, long position) {
        super(header, archive, position);
    }

    @Override
    public void accept(TarVisitor visitor) {
        visitor.visitSymbolicLinkFile(this);
    }
}