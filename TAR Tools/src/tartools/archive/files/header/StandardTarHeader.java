package tartools.archive.files.header;

import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public class StandardTarHeader implements TarHeader {

    protected Map<Field, String> values;

    public StandardTarHeader(Map<Field, String> values) {
        this.values = values;
    }

    @Override
    public String getName() {
        return values.get(Field.NAME);
    }

    @Override
    public int getMode() {
        return Integer.parseInt(values.get(Field.MODE), 8);
    }

    @Override
    public int getUid() {
        return Integer.parseInt(values.get(Field.UID), 8);
    }

    @Override
    public int getGid() {
        return Integer.parseInt(values.get(Field.GID), 8);
    }

    @Override
    public long getFileSize() {
        return Long.parseLong(values.get(Field.SIZE), 8);
    }

    @Override
    public long getMtime() {
        return Long.parseLong(values.get(Field.MTIME), 8);
    }

    @Override
    public long getChksum() {
        return Long.parseLong(values.get(Field.CHKSUM), 8);
    }

    @Override
    public Type getType() {
        return Type.getType(Integer.parseInt(values.get(Field.TYPE)));
    }

    @Override
    public String getLinkname() {
        return values.get(Field.LINKNAME);
    }

    @Override
    public String getMagic() {
        return values.get(Field.MAGIC);
    }

    @Override
    public String getVersion() {
        return values.get(Field.VERSION);
    }

    @Override
    public String getUname() {
        return values.get(Field.UNAME);
    }

    @Override
    public String getGname() {
        return values.get(Field.GNAME);
    }

    @Override
    public int getDevMajor() {
        if (values.get(Field.DEVMAJOR).isEmpty()) {
            return -1;
        }

        return Integer.parseInt(values.get(Field.DEVMAJOR), 8);
    }

    @Override
    public int getDevMinor() {
        if (values.get(Field.DEVMINOR).isEmpty()) {
            return -1;
        }

        return Integer.parseInt(values.get(Field.DEVMINOR), 8);
    }

    @Override
    public String getPrefix() {
        return values.get(Field.PREFIX);
    }

    @Override
    public Set<PosixFilePermission> getPosixFilePermissions() {
        final PosixFilePermission[] PERMISSIONS = PosixFilePermission.values();

        final int PERMISSIONS_LENGTH = PERMISSIONS.length;
        final int INT_MODE_MAX = (1 << PERMISSIONS_LENGTH) - 1;

        final Set<PosixFilePermission> set = EnumSet.noneOf(PosixFilePermission.class);


        int intMode = getMode();
        if ((intMode & INT_MODE_MAX) != intMode) {
            return set;
        }

        for (int i = 0; i < PERMISSIONS_LENGTH; i++) {
            if ((intMode & 1) == 1) {
                set.add(PERMISSIONS[PERMISSIONS_LENGTH - i - 1]);
            }
            intMode >>= 1;
        }

        return set;
    }

    @Override
    public Map<Field, String> getValues() {
        return values;
    }
}