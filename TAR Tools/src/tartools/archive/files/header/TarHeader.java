package tartools.archive.files.header;

import java.nio.file.attribute.PosixFilePermission;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public interface TarHeader {
    String getName();
    int getMode();
    int getUid();
    int getGid();
    long getFileSize();
    long getMtime();
    long getChksum();
    Type getType();
    String getLinkname();
    String getMagic();
    String getVersion();
    String getUname();
    String getGname();
    int getDevMajor();
    int getDevMinor();
    String getPrefix();
    Set<PosixFilePermission> getPosixFilePermissions();
    Map<Field, String> getValues();

    // Enum qui couvre l'ensemble des implémentations possibles
    enum Field {
        NAME,
        MODE,
        UID,
        GID,
        SIZE,
        MTIME,
        CHKSUM,
        TYPE,
        LINKNAME,
        MAGIC,
        VERSION,
        UNAME,
        GNAME,
        DEVMAJOR,
        DEVMINOR,
        PREFIX
    }

    enum Type {
        STANDARD_FILE(0),
        HARD_LINK(1),
        SYMBOLIC_LINK(2),
        SPECIAL_CHARACTER_FILE(3),
        SPECIAL_BLOCK_FILE(4),
        DIRECTORY(5),
        NAMED_PIPE(6),
        CONTIGUOUS_FILE(7);

        int value;

        Type(int value) {
            this.value = value;
        }

        public static Type getType(int value) {
            for (Type type : values()) {
                if (type.value == value) {
                    return type;
                }
            }
            return null;
        }
    }
}