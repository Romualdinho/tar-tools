package tartools.archive.files.header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TarHeaderFactory {

    public TarHeader createTarHeader(byte[] content) throws IOException {
        Map<TarHeader.Field, byte[]> properties = new LinkedHashMap<>();

        properties.put(TarHeader.Field.NAME, new byte[100]);
        properties.put(TarHeader.Field.MODE, new byte[8]);
        properties.put(TarHeader.Field.UID, new byte[8]);
        properties.put(TarHeader.Field.GID, new byte[8]);
        properties.put(TarHeader.Field.SIZE, new byte[12]);
        properties.put(TarHeader.Field.MTIME, new byte[12]);
        properties.put(TarHeader.Field.CHKSUM, new byte[8]);
        properties.put(TarHeader.Field.TYPE, new byte[1]);
        properties.put(TarHeader.Field.LINKNAME, new byte[100]);
        properties.put(TarHeader.Field.MAGIC, new byte[6]);
        properties.put(TarHeader.Field.VERSION, new byte[2]);
        properties.put(TarHeader.Field.UNAME, new byte[32]);
        properties.put(TarHeader.Field.GNAME, new byte[32]);
        properties.put(TarHeader.Field.DEVMAJOR, new byte[8]);
        properties.put(TarHeader.Field.DEVMINOR, new byte[8]);
        properties.put(TarHeader.Field.PREFIX, new byte[155]);

        ByteArrayInputStream bas = new ByteArrayInputStream(content);
        for (byte[] property : properties.values()) {
            bas.read(property, 0, property.length);
        }
        bas.close();

        Map<TarHeader.Field, String> values = new HashMap<>();
        for (Map.Entry<TarHeader.Field, byte[]> entry : properties.entrySet()) {
            values.put(entry.getKey(), new String(entry.getValue()).trim());
        }

        return new StandardTarHeader(values);
    }
}