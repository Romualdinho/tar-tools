package tartools.archive.visitors;

import tartools.archive.files.FileRepresentation;
import tartools.options.TarOptionWrapper;
import tartools.options.filter.TarFilterOption;
import tartools.options.standard.TarStandardOption;

@SuppressWarnings("unused")
public abstract class AbstractOptionalTarVisitor implements TarVisitor {

    private TarOptionWrapper optionsWrapper;

    public AbstractOptionalTarVisitor(TarOptionWrapper optionsWrapper) {
        this.optionsWrapper = optionsWrapper;
    }

    public boolean canVisit(FileRepresentation file) {
        for (TarFilterOption option : optionsWrapper.getFilterOptions()) {
            if (!option.filter(file.getHeader())) {
                return false;
            }
        }

        return true;
    }

    public void executeOptions(FileRepresentation file) {
        for (TarStandardOption option : optionsWrapper.getStandardOptions()) {
            option.execute(file.getHeader());
        }
    }

    public TarOptionWrapper getOptions() {
        return optionsWrapper;
    }
}