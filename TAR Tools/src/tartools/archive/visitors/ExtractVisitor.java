package tartools.archive.visitors;

import tartools.archive.files.*;
import tartools.options.TarOptionWrapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public class ExtractVisitor extends AbstractOptionalTarVisitor {

    private File target;

    public ExtractVisitor(File target, TarOptionWrapper wrapper) {
        super(wrapper);

        if (!target.exists()) {
            throw new IllegalArgumentException("Target directory doesn't exist");
        }

        if (!target.isDirectory()) {
            throw new IllegalArgumentException("Target must be a directory");
        }

        this.target = target;
    }

    @Override
    public void visitStandardFile(StandardFileRepresentation file) {
        if (!canVisit(file)) {
            return;
        }

        try {
            // Récupère le chemin du fichier extrait
            Path path = Paths.get(target.getAbsolutePath() + "/" + file.getHeader().getName());

            // On supprime le fichier s'il existe déjà dans le répertoire de destination
            Files.deleteIfExists(path);

            // Assure que les dossiers parents ont été créés auparavant
            ensureParentExists(path);

            // Création du fichier
            Files.createFile(path);

            // Ouverture d'un flux d'écriture afin d'écrire le contenu du fichier précédemment créé
            FileOutputStream fos = new FileOutputStream(path.toFile());

            // Ouverture d'un flux de lecture de l'archive afin de lire son contenu
            FileInputStream fis = new FileInputStream(file.getArchive());
            FileChannel channel = fis.getChannel();

            // Position (par rapport à l'archive tar) à laquelle le contenu du fichier commence
            long start = file.getFilePosition();

            // Position (par rapport à l'archive tar) à laquelle le contenu du se termine
            long end = start + file.getHeader().getFileSize();

            // Position du flux
            channel.position(start);

            // On recupère le contenu du fichier pour le réécrire dans le fichier précédémment créé
            byte[] buf = new byte[(int) file.getHeader().getFileSize()];
            fis.read(buf);
            fos.write(buf);

            // Fermeture des flux de lecture/écriture
            fis.close();
            fos.close();

            // Execution des options standard
            executeOptions(file);

            // Modification (si possible) des attributs (lastModifiedTime, owner, permissions)
            updateFileAttributes(path, file);
        } catch (Exception e) {
            // rien
        }
    }

    @Override
    public void visitDirectoryFile(DirectoryFileRepresentation directory) {
        if (!canVisit(directory)) {
            return;
        }

        try {
            // Récupère le chemin du fichier extrait
            Path path = Paths.get(target.getAbsolutePath() + "/" + directory.getHeader().getName());

            // Assure que les dossiers parents ont été créés auparavant
            ensureParentExists(path);

            // Création du répertoire
            if (!path.toFile().exists()) {
                Files.createDirectory(path);
            }

            // Execution des options standard
            executeOptions(directory);

            // Modification (si possible) des attributs (lastModifiedTime, owner, permissions)
            updateFileAttributes(path, directory);
        } catch (Exception e) {
            // rien
        }
    }

    @Override
    public void visitSymbolicLinkFile(SymbolicLinkRepresentation link) {
        if (!canVisit(link)) {
            return;
        }

        try {
            // Récupère le chemin du fichier extrait
            Path path = Paths.get(target.getAbsolutePath() + "/" + link.getHeader().getName());

            // On supprime le fichier s'il existe déjà dans le répertoire de destination
            Files.deleteIfExists(path);

            // Assure que les dossiers parents ont été créés auparavant
            ensureParentExists(path);

            // Création du lien symbolique
            Files.createSymbolicLink(path, Paths.get(link.getHeader().getLinkname()));

            // Execution des options standard
            executeOptions(link);

            // Modification (si possible) des attributs (lastModifiedTime, owner, permissions)
            updateFileAttributes(path, link);
        } catch (Exception e) {
            // rien
        }
    }

    @Override
    public void visitMaterialLinkFile(HardLinkRepresentation link) {
        if (!canVisit(link)) {
            return;
        }

        try {
            // Récupère le chemin du fichier extrait
            Path path = Paths.get(target.getAbsolutePath() + "/" + link.getHeader().getName());

            // On supprime le fichier s'il existe déjà dans le répertoire de destination
            Files.deleteIfExists(path);

            // Assure que les dossiers parents ont été créés auparavant
            ensureParentExists(path);

            // Création du lien matériel
            Files.createLink(path, Paths.get(link.getHeader().getLinkname()));

            // Execution des options standard
            executeOptions(link);

            // Modification (si possible) des attributs (lastModifiedTime, owner, permissions)
            updateFileAttributes(path, link);
        } catch (Exception e) {
            // rien
        }
    }

    public File getTargetDirectory() {
        return target;
    }

    private void ensureParentExists(Path path) {
        // Si le dossier parent n'existe pas, on le créé
        if (!path.getParent().toFile().exists()) {
            path.getParent().toFile().mkdirs();
        }
    }

    private void updateFileAttributes(Path path, FileRepresentation file) throws Exception {
        // Modification de la date de dernière modification
        Files.setLastModifiedTime(path, FileTime.from(file.getHeader().getMtime(), TimeUnit.SECONDS));

        // Modification des permissions
        Files.setPosixFilePermissions(path, file.getHeader().getPosixFilePermissions());

        PosixFileAttributeView view = Files.getFileAttributeView(path, PosixFileAttributeView.class);
        if (view != null) {
            // Modification (si possible) du nom du propriétaire ainsi que du groupe
            UserPrincipalLookupService lookupService = path.getFileSystem().getUserPrincipalLookupService();
            view.setOwner(lookupService.lookupPrincipalByName(file.getHeader().getUname()));
            view.setGroup(lookupService.lookupPrincipalByGroupName(file.getHeader().getGname()));
        }
    }
}