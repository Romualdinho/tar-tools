package tartools.archive.visitors;

import tartools.archive.files.*;
import tartools.archive.files.header.TarHeader;
import tartools.options.TarOptionWrapper;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ListVisitor extends AbstractOptionalTarVisitor {

    public ListVisitor(TarOptionWrapper wrapper) {
        super(wrapper);
    }

    @Override
    public void visitStandardFile(StandardFileRepresentation file) {
        if (!canVisit(file)) {
            return;
        }

        print(file);
    }

    @Override
    public void visitDirectoryFile(DirectoryFileRepresentation directory) {
        if (!canVisit(directory)) {
            return;
        }

        print(directory);
    }

    @Override
    public void visitSymbolicLinkFile(SymbolicLinkRepresentation link) {
        if (!canVisit(link)) {
            return;
        }

        print(link);
    }

    @Override
    public void visitMaterialLinkFile(HardLinkRepresentation link) {
        if (!canVisit(link)) {
            return;
        }

        print(link);
    }

    private void print(FileRepresentation file) {
        Path path = Paths.get(file.getHeader().getName());

        String tabs = "";
        for (int i = 0; i < path.getNameCount() - 1; i++) {
            tabs += '\t';
        }
        if (file.getHeader().getType() == TarHeader.Type.DIRECTORY) {
            System.out.print(tabs + "[" + path.getFileName() + "] ");
        } else {
            System.out.print(tabs + path.getFileName() + " ");
        }

        executeOptions(file);

        System.out.println();
    }
}