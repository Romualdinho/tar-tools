package tartools.archive.visitors;

import tartools.archive.files.DirectoryFileRepresentation;
import tartools.archive.files.HardLinkRepresentation;
import tartools.archive.files.StandardFileRepresentation;
import tartools.archive.files.SymbolicLinkRepresentation;

@SuppressWarnings("unused")
public interface TarVisitor {
    void visitStandardFile(StandardFileRepresentation file);
    void visitDirectoryFile(DirectoryFileRepresentation directory);
    void visitSymbolicLinkFile(SymbolicLinkRepresentation link);
    void visitMaterialLinkFile(HardLinkRepresentation link);
}