package tartools.options;

import tartools.options.filter.*;
import tartools.options.standard.PrintPathOption;
import tartools.options.standard.TarStandardOption;

public class ExtractOptionFactory implements TarOptionFactory {
    @Override
    public TarStandardOption createStandardOption(String name) {
        switch (name) {
            case "v":
                return new PrintPathOption();
        }

        throw new IllegalArgumentException(name + " is not supported");
    }

    @Override
    public TarFilterOption createFilterOption(String name, String[] parameters) {
        switch (name) {
            case "n":
                return new PathNameFilterOption(parameters[0]);
            case "nx":
                return new RegexFilterOption(parameters[0]);
            case "u":
                return new NameFilterOption(parameters[0], NameFilterOption.Type.OWNER);
            case "g":
                return new NameFilterOption(parameters[0], NameFilterOption.Type.GROUP);
            case "ui":
                return new IdFilterOption(Integer.parseInt(parameters[0]), IdFilterOption.Type.OWNER);
            case "gi":
                return new IdFilterOption(Integer.parseInt(parameters[0]), IdFilterOption.Type.GROUP);
            case "sl":
                return new SizeFilterOption(Long.parseLong(parameters[0]), SizeFilterOption.SizeOperator.LOWER);
            case "sg":
                return new SizeFilterOption(Long.parseLong(parameters[0]), SizeFilterOption.SizeOperator.GREATER);
            case "se":
                return new SizeFilterOption(Long.parseLong(parameters[0]), SizeFilterOption.SizeOperator.EQUAL);
        }

        throw new IllegalArgumentException(name + " is not supported");
    }
}