package tartools.options;

import tartools.options.filter.TarFilterOption;
import tartools.options.standard.TarStandardOption;

public interface TarOptionFactory {
    TarStandardOption createStandardOption(String name);
    TarFilterOption createFilterOption(String name, String[] parameters);
}