package tartools.options;

import tartools.options.filter.TarFilterOption;
import tartools.options.standard.TarStandardOption;

import java.util.Collection;
import java.util.LinkedHashSet;

public class TarOptionWrapper {

    private Collection<TarStandardOption> options;
    private Collection<TarFilterOption> filters;

    public TarOptionWrapper() {
        options = new LinkedHashSet<>();
        filters = new LinkedHashSet<>();
    }

    public TarOptionWrapper(Collection<TarStandardOption> options, Collection<TarFilterOption> filters) {
        this.options = options;
        this.filters = filters;
    }

    public void addOption(TarStandardOption option) {
        options.add(option);
    }

    public void addOption(TarFilterOption option) {
        filters.add(option);
    }

    public void removeOption(TarStandardOption option) {
        options.remove(option);
    }

    public void removeOption(TarFilterOption option) {
        filters.remove(option);
    }

    public Collection<TarStandardOption> getStandardOptions() {
        return options;
    }

    public Collection<TarFilterOption> getFilterOptions() {
        return filters;
    }
}
