package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

@SuppressWarnings("unused")
public class IdFilterOption implements TarFilterOption {

    private int id;
    private Type type;

    public IdFilterOption(int id, Type type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public boolean filter(TarHeader header) {
        return id == (type == Type.OWNER ? header.getUid() : header.getGid());
    }

    public int getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        OWNER, GROUP
    }
}
