package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

@SuppressWarnings("unused")
public class NameFilterOption implements TarFilterOption {

    private String name;
    private Type type;

    public NameFilterOption(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public boolean filter(TarHeader header) {
        return name.equals(type == Type.OWNER ? header.getUname() : header.getGname());
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        OWNER, GROUP
    }
}
