package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

import java.nio.file.Path;
import java.nio.file.Paths;

@SuppressWarnings("unused")
public class PathNameFilterOption implements TarFilterOption {

    private Path requiredPath;

    public PathNameFilterOption(String pathname) {
        this.requiredPath = Paths.get(pathname);
    }

    @Override
    public boolean filter(TarHeader header) {
        Path filePath = Paths.get(header.getName());
        int max = Math.min(requiredPath.getNameCount(), filePath.getNameCount());

        for (int i = 0; i < max; i++) {
            if (!requiredPath.getName(i).equals(filePath.getName(i))) {
                return false;
            }
        }

        return true;
    }

    public Path getPath() {
        return requiredPath;
    }
}