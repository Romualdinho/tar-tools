package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class RegexFilterOption implements TarFilterOption {

    private Pattern pattern;

    public RegexFilterOption(String regex) {
        pattern = Pattern.compile(regex);
    }

    @Override
    public boolean filter(TarHeader header) {
        return pattern.matcher(header.getName()).matches();
    }

    public Pattern getPattern() {
        return pattern;
    }
}
