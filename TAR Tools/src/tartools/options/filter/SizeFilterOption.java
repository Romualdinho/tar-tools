package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

@SuppressWarnings("unused")
public class SizeFilterOption implements TarFilterOption {


    private long size;
    private SizeOperator operator;

    public SizeFilterOption(long size, SizeOperator operator) {
        this.size = size;
        this.operator = operator;
    }

    @Override
    public boolean filter(TarHeader header) {
        if (header.getType() == TarHeader.Type.DIRECTORY) {
            return true;
        }

        switch (operator) {
            case LOWER:
                return header.getFileSize() < size;
            case GREATER:
                return header.getFileSize() > size;
            case EQUAL:
                return header.getFileSize() == size;
        }

        return false;
    }

    public long getSize() {
        return size;
    }

    public SizeOperator getOperator() {
        return operator;
    }

    public enum SizeOperator {
        LOWER, GREATER, EQUAL
    }
}