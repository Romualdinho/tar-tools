package tartools.options.filter;

import tartools.archive.files.header.TarHeader;

public interface TarFilterOption {
    boolean filter(TarHeader header);
}
