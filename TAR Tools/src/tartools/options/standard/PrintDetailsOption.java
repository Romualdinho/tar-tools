package tartools.options.standard;

import tartools.archive.files.header.TarHeader;

import java.nio.file.attribute.PosixFilePermissions;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class PrintDetailsOption implements TarStandardOption {
    @Override
    public void execute(TarHeader header) {
        String details;

        TarHeader.Type type = header.getType();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        switch (type) {
            case DIRECTORY:
                details = String.format("(uid=%s(%d),gid=%s(%d),mode=%s)",
                        header.getUname(), header.getUid(), header.getGname(), header.getGid(),
                        PosixFilePermissions.toString(header.getPosixFilePermissions()));
                break;
            case SYMBOLIC_LINK:
            case HARD_LINK:
                details = String.format("(linkname=%s,date=%s,uid=%s(%d),gid=%s(%d),mode=%s)",
                        header.getLinkname(), sdf.format(Date.from(Instant.ofEpochSecond(header.getMtime()))),
                        header.getUname(), header.getUid(), header.getGname(), header.getGid(),
                        PosixFilePermissions.toString(header.getPosixFilePermissions()));
                break;
            default:
                details = String.format("(size=%d,date=%s,uid=%s(%d),gid=%s(%d),mode=%s)",
                        header.getFileSize(), sdf.format(Date.from(Instant.ofEpochSecond(header.getMtime()))),
                        header.getUname(), header.getUid(), header.getGname(), header.getGid(),
                        PosixFilePermissions.toString(header.getPosixFilePermissions()));
                break;
        }

        System.out.print(details);
    }
}