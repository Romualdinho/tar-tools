package tartools.options.standard;

import tartools.archive.files.header.TarHeader;

public class PrintPathOption implements TarStandardOption {
    @Override
    public void execute(TarHeader header) {
        System.out.println(header.getName());
    }
}