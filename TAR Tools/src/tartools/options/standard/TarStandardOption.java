package tartools.options.standard;

import tartools.archive.files.header.TarHeader;

public interface TarStandardOption {
    void execute(TarHeader header);
}
